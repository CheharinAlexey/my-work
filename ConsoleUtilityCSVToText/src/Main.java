import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        try {
            List<String> stringsPaths = new ArrayList<>(Arrays.asList(args));
            Writer writer = new Writer();
            for (String str : stringsPaths) {
                Thread thread1 = new Thread(() -> {
                    try {
                        Reader reader = new Reader(str);
                        reader.readAndCreateFile();
                        writer.doWriteToFiles(reader.getOutputFiles(),reader.getLinesIn());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                thread1.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
