import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Reader {
    private final List<File> outputFiles = new ArrayList<>();
    private final List<String> linesIn;

    public Reader(String pathIn) throws IOException {
        this.linesIn = Files.readAllLines(Paths.get(pathIn));
    }

    public synchronized void readAndCreateFile() {
        String[] filesName = linesIn.get(0).split(";");
        for (String str : filesName) {
            File file = new File("./data/" + str + ".txt");
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            outputFiles.add(file);
        }
    }

    public List<File> getOutputFiles() {
        return outputFiles;
    }

    public List<String> getLinesIn() {
        return linesIn;
    }
}
