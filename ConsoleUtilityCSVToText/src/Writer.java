import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Writer {

    private final Map<String,List<String>> fileNameAndStringsIn = new HashMap<>();


    public void doWriteToFiles( List<File> outputFiles,List<String> linesIn) {
        int count = 0;
        for (File file : outputFiles) {
            synchronized (fileNameAndStringsIn) {
                List<String> values = new ArrayList<>();
                for (int i = 1; i < linesIn.size(); i++) {
                    String[] nextLine = linesIn.get(i).split(";");
                    String value = nextLine[count];
                    if (!values.contains(value)) {
                        values.add(value);
                    }
                }
                fileNameAndStringsIn.put(createFileNameWithoutExtension(file), values);
                count++;
                try {
                    writeToFile(file, fileNameAndStringsIn);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String toString(List<String> strings) {
        StringBuilder builder = new StringBuilder();
        for (String str : strings) {
            builder.append(str).append(";");
        }
        return builder.toString();
    }

    private synchronized void writeToFile(File file,Map<String,List<String>> map) throws IOException {
        for (Map.Entry<String,List<String>> entry : map.entrySet()) {
            String key = entry.getKey();
            List<String> values= entry.getValue();
            if (createFileNameWithoutExtension(file).equals(key)) {
                BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
                writer.write( createFileNameWithoutExtension(file)+ ":\n" + toString(values) + "\n");
                writer.flush();
                writer.close();
            }
        }
    }


    private String createFileNameWithoutExtension(File file) {
        String[] fileWithoutExtension = file.getName().split("\\.");
        return fileWithoutExtension[0];
    }

}
