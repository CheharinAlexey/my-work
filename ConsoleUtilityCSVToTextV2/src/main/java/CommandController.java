import commands.Commands;
import commands.ExitCommand;
import commands.HelpCommand;
import commands.MainCommand;

public class CommandController {
    private Commands commands;
    private boolean isExit = false;

    public void runCommands(String command) {
        switch (command) {
            case "-m":
                commands = new MainCommand();
                break;
            case "-h":
                commands = new HelpCommand();
                break;
            case "-e":
                commands = new ExitCommand();
                isExit = true;
                break;
            default:
                commands = new HelpCommand();
                break;
        }
        commands.run();
    }

    public boolean isExit() {
        return isExit;
    }
}
