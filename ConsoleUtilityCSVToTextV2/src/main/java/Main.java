import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {

        System.out.println("Привет друг! тебя приветсвует программа перевода CSV файлов в TXT файлы!");

        while (true) {
            CommandController commandController = new CommandController();
            String command = scanner.nextLine();
            commandController.runCommands(command);
            if (commandController.isExit()) {
                break;
            }
        }

    }
}
