package commands;

public class ExitCommand implements Commands {


    @Override
    public void run() {
        System.out.println("Надеюсь, что вам понравилась программа!\n"
                + "До встречи!");
    }

}
