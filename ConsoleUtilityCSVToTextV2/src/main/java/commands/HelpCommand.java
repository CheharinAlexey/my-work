package commands;

public class HelpCommand implements Commands {
    private String command = "-m - начать выполнение основной команды\n" +
            "-e - выйти из программы\n" +
            "-h - получить справку по командам";

    @Override
    public void run() {
        System.out.println(command);
    }
}
