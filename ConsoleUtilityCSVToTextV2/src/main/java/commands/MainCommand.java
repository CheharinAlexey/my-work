package commands;

import commands.exeptions.InputDataExceptions;
import commands.mainCommand.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;

public class MainCommand implements Commands {

    private final Scanner in = new Scanner(System.in);
    private final String inputData = "Есть два способа ввода данные: " +
            "1. ПутьКПапкеГдеБудутСоздаватьсяФайлы ПУтьКФайлу..." +
            "2. ПутьКФайлам";


    @Override
    public void run() {
        System.out.println(inputData);
        String[] arg = in.nextLine().split(" ");
        List<Thread> sessionsThreads = new ArrayList<>();
        try {
            if (CheckInputData.check(arg)) {
                for (String pathToFile : CheckInputData.getRightPathsToFiles()) {
                    sessionsThreads.add(createSessionReadCreateCollect(pathToFile));
                }
                while (sessionsThreadsIsWork(sessionsThreads)) {

                }
                Writer.writeToFiles(Collector.getFileAndValue());
                Collector.cleanFileAndValue();
                System.out.println("Все сделано, можно проверять!");
            } else {
                System.out.println("Упс, что то пошло не так, попробуйте заново");
            }
        } catch (InputDataExceptions ex) {
            System.out.println(ex.getErrorCode().getErrorCode());
        }
    }

    private Thread createSessionReadCreateCollect(String pathToFile) {

        Thread thread = new Thread(() -> {
            try {
                Reader reader = new Reader();
                Collector collector = new Collector();
                Creator creator = new Creator();
                reader.readFile(pathToFile);
                creator.createFiles(reader.getFilesName(), CheckInputData.getPathToDir());
                collector.collect(creator.getCreatedFiles(), reader.getLinesIn(), reader.getFilesName());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        thread.start();
        return thread;
    }

    private boolean sessionsThreadsIsWork(List<Thread> sessionsThreads) {
        for (Thread session : sessionsThreads) {
            if (session.isAlive()) {
                return true;
            }
        }
        return false;
    }
}
