package commands.exeptions;

public enum InputDataErrorCode {
    WRONG_PATH_TO_DIR("Не верно введен путь к папке"),
    WRONG_PATH_TO_FILE("Не верно введен путь к файлам");

    private String errorCode;

    InputDataErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
