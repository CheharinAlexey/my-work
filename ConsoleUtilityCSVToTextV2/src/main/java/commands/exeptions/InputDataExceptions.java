package commands.exeptions;

public class InputDataExceptions extends Exception {
    private final InputDataErrorCode errorCode;

    public InputDataExceptions(InputDataErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public InputDataErrorCode getErrorCode() {
        return errorCode;
    }
}
