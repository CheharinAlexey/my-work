package commands.mainCommand;

import commands.exeptions.InputDataErrorCode;
import commands.exeptions.InputDataExceptions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CheckInputData {
    private static final List<String> rightPathsToFiles = new ArrayList<>();
    private static String pathToDir;

    public static boolean check(String[] args) throws InputDataExceptions {
        if (checkPathToDir(args)) {
            checkPathsToFilesWithDir(args);
        } else {
            checkPathsToFilesWithoutDir(args);
        }
        return !rightPathsToFiles.isEmpty();
    }

    private static boolean checkPathToDir(String[] args) throws InputDataExceptions {
        if (!new File(args[0]).isDirectory()) {
            System.out.println("Файлы будут созданы в стандартной папке");
            pathToDir = "data/";
            return false;
        } else {
            System.out.println("Создание файлов произойдет в: " + args[0]);
            pathToDir = args[0];
            return true;
        }
    }

    private static void checkPathsToFilesWithDir(String[] args) throws InputDataExceptions {
        for (int i = 1; i < args.length;i++) {
            String path = args[i];
            if (new File(path).exists() || path.endsWith(".csv")) {
                rightPathsToFiles.add(path);
            } else {
                throw new InputDataExceptions(InputDataErrorCode.WRONG_PATH_TO_FILE);
            }
        }
    }

    private static void checkPathsToFilesWithoutDir(String[] args) throws InputDataExceptions {
        for (int i = 0; i < args.length;i++) {
            String path = args[i];
            if (new File(path).exists() || path.endsWith(".csv")) {
                rightPathsToFiles.add(path);
            } else {
                throw new InputDataExceptions(InputDataErrorCode.WRONG_PATH_TO_FILE);
            }
        }
    }

    public static List<String> getRightPathsToFiles() {
        return rightPathsToFiles;
    }

    public static String getPathToDir() {
        return pathToDir;
    }
}
