package commands.mainCommand;

import java.io.File;
import java.util.*;

public class Collector {
    private static final Map<File, Set<String>> fileAndValue = new HashMap<>();

    public synchronized void collect(List<File> filesToWrite, List<String> linesToWrite, String[] filesName) {
        int count = 0;
        for (File file : filesToWrite) {
            System.out.println(file.getName());
            if (!Arrays.asList(filesName).contains(file.getName().split("\\.")[0])) {
                continue;
            }
            System.out.println("Собираю значения");
            Set<String> values = collectValue(linesToWrite, count);
            System.out.println(values);
            addFileAndValues(file, values);
            count++;
        }

    }

    private Set<String> collectValue(List<String> linesToWrite, int count) {
        Set<String> values = new HashSet<>();
        for (String line : linesToWrite) {
            String[] nextLine = line.split(";");
            String value = nextLine[count];
            values.add(value);
        }
        return values;
    }

    private void addFileAndValues(File file, Set<String> values) {
        if (fileAndValue.containsKey(file)) {
            Set<String> valuesFromCreatedFile = fileAndValue.get(file);
            valuesFromCreatedFile.addAll(values);
            fileAndValue.put(file, valuesFromCreatedFile);
        } else {
            fileAndValue.put(file, values);
        }
    }

    public static Map<File, Set<String>> getFileAndValue() {
        return fileAndValue;
    }

    public static void cleanFileAndValue() {
        fileAndValue.clear();
    }
}
