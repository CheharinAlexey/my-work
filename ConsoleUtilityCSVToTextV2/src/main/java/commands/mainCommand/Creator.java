package commands.mainCommand;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Creator {
    private final List<File> createdFiles = new ArrayList<>();

    public void createFiles(String[] filesName, String dirForFiles) throws IOException {
        for (String name : filesName) {
            File file = new File(dirForFiles + name + ".txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            createdFiles.add(file);
            System.out.println(file.getAbsolutePath());
        }
    }

    public List<File> getCreatedFiles() {
        return createdFiles;
    }
}
