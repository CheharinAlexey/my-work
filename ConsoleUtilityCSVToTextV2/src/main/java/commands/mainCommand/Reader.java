package commands.mainCommand;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Reader {
    private List<String> linesToWrite;
    private String[] filesName;

    public Reader() {
        linesToWrite = new ArrayList<>();
    }

    public void readFile(String path) throws IOException {
        System.out.println("Читаю строчки в " + path);
        linesToWrite = Files.readAllLines(Paths.get(path));
        System.out.println(linesToWrite);
        filesName = linesToWrite.get(0).split(";");
        linesToWrite.remove(0);
    }

    public String[] getFilesName() {
        return filesName;
    }

    public List<String> getLinesIn() {
        return linesToWrite;
    }
}
