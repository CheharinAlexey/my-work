package commands.mainCommand;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Set;


public class Writer {

    public static void writeToFiles(Map<File, Set<String>> fileAndValue) {
        System.out.println("Запись пошла");
        for (Map.Entry<File, Set<String>> entry : fileAndValue.entrySet()) {
            File file = entry.getKey();
            Set<String> values = entry.getValue();
            System.out.println("Записываю значения " + values + " в файл " + file);
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
                System.out.println(values);
                writer.write(toString(values));
                writer.flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private static String toString(Set<String> strings) {
        StringBuilder builder = new StringBuilder();
        for (String str : strings) {
            builder.append(str).append(";");
        }
        return builder.toString();
    }

}
