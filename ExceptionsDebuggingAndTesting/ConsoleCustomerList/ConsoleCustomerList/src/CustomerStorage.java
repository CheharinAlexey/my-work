import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomerStorage
{
    private HashMap<String, Customer> storage;
    private Pattern namePattern = Pattern.compile("^([a-zA-Zа-яА-Я]*\\s?[a-zA-Zа-яА-Я]*)");
    private Pattern phonePattern = Pattern.compile("(\\d*-*\\(*\\)*)");
    private Pattern emailPattern = Pattern.compile("(\\w+\\.*@+\\w+\\.+\\w+)*");

    public CustomerStorage()
    {
        storage = new HashMap<>();
    }

    public void addCustomer(String data) throws EMailException {
        String[] components = data.split("\\s+");
        String name = components[0] + " " + components[1];
        Matcher nameMatcher = namePattern.matcher(name);
        Matcher phoneMatcher = phonePattern.matcher(components[3]);
        Matcher emailMatcher = emailPattern.matcher(components[2]);
        if (!nameMatcher.matches())
            throw new IllegalArgumentException("Вы ввели неверное имя!");
        if (!phoneMatcher.matches())
            throw new PhoneNumberIsNotAvailableException("Вы ввели телефон в неверном формате!");
        if (!emailMatcher.matches())
            throw new EMailException("Вы ввели неверный email-адрес!");
        else
            storage.put(name, new Customer(name, components[3], components[2]));
    }

    public void listCustomers()
    {
        storage.values().forEach(System.out::println);
    }

    public void removeCustomer(String name)
    {
        Matcher nameMatcher = namePattern.matcher(name);
        if (!nameMatcher.matches())
            throw new IllegalArgumentException("Вы ввели неверное имя!");
        else
        storage.remove(name);
    }

    public int getCount()
    {
        return storage.size();
    }
}