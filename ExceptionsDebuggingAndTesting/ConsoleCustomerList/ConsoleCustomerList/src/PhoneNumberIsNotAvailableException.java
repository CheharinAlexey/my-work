public class PhoneNumberIsNotAvailableException extends RuntimeException {
    public PhoneNumberIsNotAvailableException(String message){
        super(message);
    }
}
