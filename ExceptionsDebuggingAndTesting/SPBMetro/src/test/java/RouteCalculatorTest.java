import core.Line;
import core.Station;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

public class RouteCalculatorTest {
    List<Station> route;
    List<Station> routeOnOneLine;
    List<Station> routeWithOneConnection;
    List<Station> routeWithTwoConnection;
    StationIndex stationIndex;
    RouteCalculator routeCalculator;
    @Before
    public void setUp() throws Exception {
        route = new ArrayList<>();
        Line line1 = new Line(1,"First");
        Line line2 = new Line(2,"Second");
        Line line3 = new Line(3,"Third");
        Line line4 = new Line(4,"Fourth");
        Line line5 = new Line(5,"Fifth");

        route.add(new Station("Петровская",line1));
        route.add(new Station("Арбузная",line1));
        route.add(new Station("Заборная",line1));
        route.add(new Station("Веселая",line2));
        route.add(new Station("Честная",line2));
        route.add(new Station("Привокзальная",line3));
        stationIndex = new StationIndex();
        stationIndex.addLine(line1);
        stationIndex.addLine(line2);
        stationIndex.addLine(line3);
        stationIndex.addLine(line4);
        stationIndex.addLine(line5);

        stationIndex.addStation(new Station("Петровская",line1));
        stationIndex.addStation(new Station("Арбузная",line1));
        stationIndex.addStation(new Station("Заборная",line1));
        stationIndex.addStation(new Station("Веселая",line2));
        stationIndex.addStation(new Station("Честная",line2));
        stationIndex.addStation(new Station("Привокзальная",line3));
        stationIndex.addStation(new Station("Скучная",line3));
        stationIndex.addStation(new Station("Везучая",line3));
        stationIndex.addStation(new Station("Последняя",line4));
        stationIndex.addStation(new Station("ЕщёОдна",line4));
        stationIndex.addStation(new Station("Временная",line5));
        stationIndex.addStation(new Station("Квадратная", line5));

        line1.addStation(stationIndex.getStation("Петровская"));
        line1.addStation(stationIndex.getStation("Арбузная"));
        line1.addStation(stationIndex.getStation("Заборная"));
        line2.addStation(stationIndex.getStation("Веселая"));
        line2.addStation(stationIndex.getStation("Честная"));
        line3.addStation(stationIndex.getStation("Привокзальная"));
        line3.addStation(stationIndex.getStation("Везучая"));
        line4.addStation(stationIndex.getStation("Последняя"));
        line4.addStation(stationIndex.getStation("ЕщёОдна"));
        line5.addStation(stationIndex.getStation("Временная"));
        line5.addStation(stationIndex.getStation("Квадратная"));

        List<Station> connections1 = new ArrayList<>();
        connections1.add(stationIndex.getStation("Арбузная"));
        connections1.add(stationIndex.getStation("Веселая"));
//        connections.add(stationIndex.getStation("Привокзальная"));
//        connections.add(stationIndex.getStation("Везучая"));
//        connections.add(stationIndex.getStation("Последняя"));
//        connections.add(stationIndex.getStation("ЕщёОдна"));
        stationIndex.addConnection(connections1);
        List<Station> connections2 = new ArrayList<>();
        connections2.add(stationIndex.getStation("Честная"));
        connections2.add(stationIndex.getStation("Привокзальная"));
        stationIndex.addConnection(connections2);
        List<Station> connections3 = new ArrayList<>();
        connections3.add(stationIndex.getStation("Скучная"));
        connections3.add(stationIndex.getStation("Последняя"));
        connections3.add(stationIndex.getStation("Временная"));
        stationIndex.addConnection(connections3);
       // List<Station> connections4 = new ArrayList<>();
//        connections4.add(stationIndex.getStation("Везучая"));
//        connections4.add(stationIndex.getStation("ЕщёОдна"));
//        stationIndex.addConnection(connections4);

        routeOnOneLine = new ArrayList<>();
        routeOnOneLine.add(stationIndex.getStation("Петровская"));
        routeOnOneLine.add(stationIndex.getStation("Арбузная"));
        routeOnOneLine.add(stationIndex.getStation("Заборная"));

        routeWithOneConnection = new ArrayList<>();
        routeWithOneConnection.add(stationIndex.getStation("Петровская"));
        routeWithOneConnection.add(stationIndex.getStation("Арбузная"));
        routeWithOneConnection.add(stationIndex.getStation("Веселая"));
        routeWithOneConnection.add(stationIndex.getStation("Честная"));

        routeWithTwoConnection = new ArrayList<>();
        routeWithTwoConnection.add(stationIndex.getStation("Последняя"));
        routeWithTwoConnection.add(stationIndex.getStation("Везучая"));
        routeWithTwoConnection.add(stationIndex.getStation("Привокзальная"));
        routeWithTwoConnection.add(stationIndex.getStation("Честная"));
    }


    @Test
    public void testCalculateDuration() {
        double actual = RouteCalculator.calculateDuration(route);
        double expected = 14.5;
        Assert.assertEquals(expected,actual,0.01);
    }
        @Test
    public void shouldReturnShortestRouteOnOneLine() {
        Station from = stationIndex.getStation("Петровская",1);
        Station to = stationIndex.getStation("Заборная",1);
        routeCalculator = new RouteCalculator(stationIndex);
        List<Station> actualRoute = routeCalculator.getShortestRoute(from,to);
        List<Station> expectedRoute = routeOnOneLine;
        Assert.assertEquals(expectedRoute,actualRoute);
    }
    @Test
    public void shouldReturnShortestRouteWithOneConnection() {
        routeCalculator = new RouteCalculator(stationIndex);
        Station from = stationIndex.getStation("Петровская",1);
        Station to = stationIndex.getStation("Честная",2);
        List<Station> actualRoute = routeCalculator.getShortestRoute(from,to);
        List<Station> expectedRoute = routeWithOneConnection;
        Assert.assertEquals(expectedRoute,actualRoute);
    }
    @Test
    public void shouldReturnShortestRouteWithTwoConnection() {
        RouteCalculator routeCalculator = new RouteCalculator(stationIndex);
        Station from = stationIndex.getStation("Последняя",4);
        Station to = stationIndex.getStation("Честная",2);
        List<Station> actualRoute = routeCalculator.getShortestRoute(from,to);
        List<Station> expectedRoute = routeWithTwoConnection;
        Assert.assertEquals(expectedRoute,actualRoute);
    }

    @After
    public void tearDown() throws Exception {

    }
}