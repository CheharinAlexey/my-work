package MetroMap;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MetroMap {
    @JsonProperty("lines")
    private List<Line> lines = new ArrayList<>();
    @JsonProperty("stations")
    private Map<String, List<Station>> stations = new HashMap<>(); // Map нужна для создания корректного json'a как это требуется в SPBMetro :)
//    private List<List<Connection>> connections = new ArrayList<>();


    public MetroMap() {

    }


    public List<Line> getLines() {
        return lines;
    }

    public void setLines(List<Line> lines) {
        this.lines = lines;
    }

    public Map<String, List<Station>> getStations() {
        return stations;
    }

    public void setStations(Map<String, List<Station>> stations) {
        this.stations = stations;
    }

    public void addMissingInformation() {
        List<Line> lineList = getLines();
        Map<String, List<Station>> mapStations = getStations();
        for (Map.Entry<String, List<Station>> map : mapStations.entrySet()) {
            List<Station> stationList = map.getValue();
            String lineNum = map.getKey();
            for (Line line : lineList) {

                if (line.getNumber().equals(lineNum)) {
                    List<Station> listStations = new ArrayList<>();
                    for (Station station : stationList) {
                        station.setLine(line);
                        listStations.add(station);
                    }
                    line.setStations(listStations);
                }
            }
        }
    }
}

