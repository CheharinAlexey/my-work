package MetroMap;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class Station implements Comparable<Station> {
    private Line line;
    private String name;

    public Station() {
    }

    public Station(Line line) {
        this.line = line;
    }
    public Station(String name) {
        this.name = name;
    }

    public Station(String name, Line line) {
        this.name = name;
        this.line = line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Line getLine() {
        return line;
    }
    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Station station) {
        int lineComparison = line.compareTo(station.getLine());
        if (lineComparison != 0) {
            return lineComparison;
        }
        return name.compareToIgnoreCase(station.getName());
    }

    @Override
    public boolean equals(Object obj) {
        return compareTo((Station) obj) == 0;
    }

    @Override
    public String toString() {
        return name;
    }
}
