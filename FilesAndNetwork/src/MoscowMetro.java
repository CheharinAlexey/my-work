import MetroMap.*;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.util.List;

import WriteMetroMap.*;

public class MoscowMetro {
    private static String path = "/home/alexei/Desktop/skillbox/java_basics/09_FilesAndNetwork/data/MSKMetroMap.json";
    private static String URL = "https://www.moscowmap.ru/metro.html#lines";
    private static Document doc = new Document(URL);

    public static void main(String[] args) {
        try {
            doc = Jsoup.connect(URL).maxBodySize(0).get();
            //WriteJson writeJson = new WriteJson();
            //writeJson.writeJson(doc, path);
            ObjectMapper objectMapper = new ObjectMapper();
            MetroMap metroMap;
            metroMap = objectMapper.readValue(new File(path), MetroMap.class);
            metroMap.addMissingInformation();
            stationsCount(metroMap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void stationsCount(MetroMap metro) throws IOException {
        List<Line> lines = metro.getLines();
        for (Line line : lines) {
            System.out.println("На линии " + line.getName() + " находится " + line.getStations().size());
        }
    }
}
