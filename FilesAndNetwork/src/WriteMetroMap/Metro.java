package WriteMetroMap;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Metro {
    private List<LineWrite> lineWrites = new ArrayList<>();
    private Map<String, List<StationWrite>> stations = new HashMap<>(); // Map нужна для создания корректного json'a как это требуется в SPBMetro :)
//    private List<List<Connection>> connections = new ArrayList<>();


    public Metro() {
    }

    public Metro(List<LineWrite> lineWrites, Map<String, List<StationWrite>> stations) {
        this.lineWrites = lineWrites;
        this.stations = stations;
    }

    public List<LineWrite> getLines() {
        return lineWrites;
    }

    public void setLines(List<LineWrite> lineWrites) {
        this.lineWrites = lineWrites;
    }

    public Map<String, List<StationWrite>> getStations() {
        return stations;
    }

    public void setStations(Map<String, List<StationWrite>> stations) {
        this.stations = stations;
    }
}
