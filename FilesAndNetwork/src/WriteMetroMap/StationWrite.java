package WriteMetroMap;

import com.fasterxml.jackson.annotation.JsonValue;

class StationWrite {

    private String name;

    public StationWrite() {
    }

    public StationWrite(String name) {
        this.name = name;

    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonValue
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
