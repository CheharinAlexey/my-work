package WriteMetroMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WriteJson {
    private List<LineWrite> lineWrites = new ArrayList<>();
    private Map<String, List<StationWrite>> stations = new HashMap<>();

    public void writeJson(Document doc, String path) {
        Elements namesOfLines = doc.getElementsByClass("js-metro-line");
        namesOfLines.forEach(el ->
                lineWrites.add
                        (new LineWrite(el.attr("data-line"), el.text())));


        Elements namesOfStations = doc.getElementsByClass("js-metro-stations t-metrostation-list-table");
        namesOfStations.forEach(el -> {
            List<StationWrite> stationsName = new ArrayList<>();
            el.children().forEach(element -> stationsName.add
                    (new StationWrite(element.getElementsByClass("name").text())));
            stations.put(el.attr("data-line"), stationsName);
        });
        Metro metro = new Metro(lineWrites, stations);
        File file = new File(path);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(file, metro);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
