import mysqlPOJO.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;


import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
        Metadata metadata = new MetadataSources(registry).getMetadataBuilder().build();
        SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();

        Session session = sessionFactory.openSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();

        CriteriaQuery<Course> courseCriteriaQuery = builder.createQuery(Course.class);
        Root<Course> courseRoot = courseCriteriaQuery.from(Course.class);
        courseCriteriaQuery.select(courseRoot);
        List<Course> courses = session.createQuery(courseCriteriaQuery).getResultList();

        List<LinkedPurchaseList> linkedPurchaseLists = new LinkedList<>();

        for (Course course : courses) {
            for (Student student : course.getStudents()) {
                linkedPurchaseLists.add(new LinkedPurchaseList(new LinkedPurchaseListId(student, course), course.getPrice()));
            }
        }
        session.beginTransaction();
        for (LinkedPurchaseList purchaseList : linkedPurchaseLists) {
            session.save(purchaseList);

        }
        session.getTransaction().commit();



        sessionFactory.close();
    }
}
