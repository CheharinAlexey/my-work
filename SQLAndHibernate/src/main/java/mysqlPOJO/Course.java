package mysqlPOJO;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Courses") // указываем имя таблицы
public class Course {
    @Id // обязательно для айдишников
    @GeneratedValue(strategy = GenerationType.IDENTITY) // автоинкремент
    @Column(name = "id", nullable = false, columnDefinition = "INT(11) UNSIGNED")
    private int id;

    private String name;

    private int duration;

    @Enumerated(EnumType.STRING) // для работы с классами ENUM
    @Column(columnDefinition = "enum(DESIGN,PROGRAMMING, MARKETING, MANAGEMENT, BUSINESS)")
    private CoursesType type;

    private String description;

    @ManyToOne(targetEntity = Teacher.class,fetch = FetchType.LAZY)
    private Teacher teacher;

    @Column(name = "students_count")
    private Integer studentsCount;

    private int price;

    @Column(name = "price_per_hour")
    private float pricePerHour;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Subscriptions",
            joinColumns = {@JoinColumn(name = "course_id")},
            inverseJoinColumns = {@JoinColumn(name = "student_id")})
    private List<Student> students;

    @OneToMany(targetEntity = Subscription.class,mappedBy = "id.course",cascade = CascadeType.ALL)
    private List<Subscription> subscriptions = new ArrayList<>();


    @OneToMany(targetEntity = LinkedPurchaseList.class,mappedBy = "id.course",cascade = CascadeType.ALL)
    private List<LinkedPurchaseList> linkedPurchaseList = new ArrayList<>();

    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }
    public void addStudent(Student student) {
        subscriptions.add(new Subscription(new SubscriptionId(this, student), new Date()));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public CoursesType getType() {
        return type;
    }

    public void setType(CoursesType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Integer getStudentsCount() {
        return studentsCount;
    }

    public void setStudentsCount(Integer studentsCount) {
        this.studentsCount = studentsCount;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public float getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(float pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public List<Student> getStudents() {
        return students;
    }

    public List<LinkedPurchaseList> getLinkedPurchaseList() {
        return linkedPurchaseList;
    }

    public void setLinkedPurchaseList(List<LinkedPurchaseList> linkedPurchaseList) {
        this.linkedPurchaseList = linkedPurchaseList;
    }
}