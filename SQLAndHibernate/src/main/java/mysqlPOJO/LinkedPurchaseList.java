package mysqlPOJO;

import javax.persistence.*;

@Entity
@Table(name = "LinkedPurchaseList")
public class LinkedPurchaseList  {
    @EmbeddedId
    private LinkedPurchaseListId id;
    @Column(nullable = false)
    private Integer price;


    public LinkedPurchaseList() {
    }

    public LinkedPurchaseList(LinkedPurchaseListId id, Integer price) {
        this.id = id;
        this.price = price;
    }

    public LinkedPurchaseListId getId() {
        return id;
    }

    public void setId(LinkedPurchaseListId id) {
        this.id = id;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

}
