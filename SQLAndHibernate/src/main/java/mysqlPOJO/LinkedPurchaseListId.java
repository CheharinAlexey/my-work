package mysqlPOJO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
public class LinkedPurchaseListId implements Serializable{
    @JoinColumn(columnDefinition = "INT UNSIGNED",nullable = false)
    @ManyToOne(targetEntity = Student.class)
    private Student student;
    @JoinColumn(columnDefinition = "INT UNSIGNED",nullable = false)
    @ManyToOne(targetEntity = Course.class)
    private Course course;

    public LinkedPurchaseListId() {
    }

    public LinkedPurchaseListId(Student student, Course course) {
        this.student = student;
        this.course = course;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinkedPurchaseListId that = (LinkedPurchaseListId) o;
        return Objects.equals(student, that.student) && Objects.equals(course, that.course);
    }

    @Override
    public int hashCode() {
        return Objects.hash(student, course);
    }
}
