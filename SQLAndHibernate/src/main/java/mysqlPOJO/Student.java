package mysqlPOJO;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Students")
public class Student {
    @Id // обязательно для айдишников
    @GeneratedValue(strategy = GenerationType.IDENTITY) // автоинкремент
    @Column(name = "id", nullable = false, columnDefinition = "INT(11) UNSIGNED")
    private int id;
    private String name;
    private int age;
    @Column(name = "registration_date")
    private Date registrationDate;


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Subscriptions",
            joinColumns = {@JoinColumn(name = "student_id")},
            inverseJoinColumns = {@JoinColumn(name = "course_id")})
    private List<Course> courses;

    @OneToMany(mappedBy = "id.student", cascade = CascadeType.ALL) // Вот тут мы уже можем использовать CascadeType.ALL так как если удаляется курс, то пускай и подписки удаляются, а если уж удаление делается каскадно, то и остальные действия тоже можно делать каскадно :)
    private List<Subscription> subscriptions = new ArrayList<>();

    @OneToMany(targetEntity = LinkedPurchaseList.class,mappedBy = "id.student",cascade = CascadeType.ALL)
    private List<LinkedPurchaseList> linkedPurchaseList;

    public Student() {
    }

    public Student(int id, String name, int age, Date registrationDate, List<Course> courses, List<Subscription> subscriptions, List<LinkedPurchaseList> linkedPurchaseList) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.registrationDate = registrationDate;
        this.courses = courses;
        this.subscriptions = subscriptions;
        this.linkedPurchaseList = linkedPurchaseList;
    }

    public void addStudent(Course course) {
        subscriptions.add(new Subscription(new SubscriptionId(course, this), new Date())); // Добавляем подписку студенту на текущий курс с текущей датой :)
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public List<LinkedPurchaseList> getLinkedPurchaseList() {
        return linkedPurchaseList;
    }

    public void setLinkedPurchaseList(List<LinkedPurchaseList> linkedPurchaseList) {
        this.linkedPurchaseList = linkedPurchaseList;
    }
}
