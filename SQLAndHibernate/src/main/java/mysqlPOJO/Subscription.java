package mysqlPOJO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Subscriptions")
public class Subscription  {
    @EmbeddedId
    private SubscriptionId id;
    @Column(name = "subscription_date")
    private Date subscriptionDate;


    public Subscription() {
    }

    public Subscription(SubscriptionId id, Date subscriptionDate) {
        this.id = id;
        this.subscriptionDate = subscriptionDate;
    }


    public SubscriptionId getId() {
        return id;
    }

    public void setId(SubscriptionId id) {
        this.id = id;
    }

    public Date getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(Date subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

}
