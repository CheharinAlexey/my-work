package mysqlPOJO;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class SubscriptionId implements Serializable {
    @ManyToOne(targetEntity = Course.class)
    @JoinColumn(name = "course_id")
    private Course course;

    @ManyToOne(targetEntity = Student.class)
    @JoinColumn(name = "student_id")
    private Student student;

    public SubscriptionId() {
    }

    public SubscriptionId(Course course, Student student) {
        this.course = course;
        this.student = student;
    }

    public Student getStudent() {
        return student;
    }


    public Course getCourse() {
        return course;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriptionId that = (SubscriptionId) o;
        return Objects.equals(course, that.course) && Objects.equals(student, that.student);
    }

    @Override
    public int hashCode() {
        return Objects.hash(course, student);
    }
}
