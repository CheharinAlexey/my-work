package com.example.springboot.controller;

import com.example.springboot.domain.ToDo;
import com.example.springboot.repos.ToDoRepo;
import com.example.springboot.services.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.Map;

@Controller
public class MainController {

    private final RepositoryService toDoRepository;

    public MainController(RepositoryService toDoRepository) {
        this.toDoRepository = toDoRepository;
    }

    @GetMapping("/")
    public String greeting(Map<String, Object> model) {
        return "greeting";
    }

    @GetMapping("/main")
    public String main(Map<String, Object> model) {
        Iterable<ToDo> toDos = toDoRepository.findAll();
        model.put("todos", toDos);
        return "main";
    }

    @PostMapping("/main")
    public String add(@RequestParam String description, @RequestParam String place,
                      Map<String, Object> model) {
        ToDo toDo = new ToDo(description, place);
        toDoRepository.save(toDo);

        Iterable<ToDo> toDos = toDoRepository.findAll();
        model.put("todos", toDos);

        return "main";
    }

    @PostMapping("filter")
    public String filter(@RequestParam String filter, Map<String,Object> model) {
        Iterable<ToDo> toDoIterable;
        if (filter != null & !filter.isEmpty()) {
             toDoIterable = toDoRepository.findByPlace(filter);
        } else {
            toDoIterable = toDoRepository.findAll();
        }

        model.put("todos",toDoIterable);
        return "main";
    }

    @DeleteMapping("/doings/")
    public ResponseEntity deleteList() {
        toDoRepository.deleteAll();
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/doings/{id}")
    public ResponseEntity deleteToDo(@PathVariable int id) throws NoHandlerFoundException {
        ToDo toDo = toDoRepository.findById(id).orElseThrow(() -> new NoHandlerFoundException("delete", "/doings/{id}", new HttpHeaders()));
        toDoRepository.delete(toDo);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PutMapping("/doings/{id}")
    public ResponseEntity putToDo(@PathVariable int id,
                                  String description,
                                  String place) throws NoHandlerFoundException {
        ToDo toDo = toDoRepository.findById(id).orElseThrow(() -> new NoHandlerFoundException("put", "/doings/{id}", new HttpHeaders()));
        toDo.setDescription(description);
        toDo.setPlace(place);
        ToDo newToDo = toDoRepository.save(toDo);
        return new ResponseEntity(newToDo, HttpStatus.OK);
    }

    @PatchMapping("/doings/{id}")
    public ResponseEntity patchToDo(ToDo toDo) throws NoHandlerFoundException {
        if (toDo == null) {
            throw new NoHandlerFoundException("patch", "/doing/{id}", new HttpHeaders());
        }
        ToDo newToDo = toDoRepository.save(toDo);
        return new ResponseEntity(newToDo, HttpStatus.OK);
    }

}
