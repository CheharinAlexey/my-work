package com.example.springboot.controller;

import com.example.springboot.domain.ToDo;
import com.example.springboot.repos.ToDoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.Map;

@Controller
public class PutController {
    @Autowired
    private ToDoRepo toDoRepository;
    @PutMapping("/put/{id}")
    public String patch(ToDo toDo, @RequestParam String description, @RequestParam String place, Map<String,Object> model) throws NoHandlerFoundException {
        ToDo newToDo = toDoRepository.findById(toDo.getId()).orElseThrow();
        model.put("todo",newToDo);
        toDo.setDescription(description);
        toDo.setPlace(place);
        toDoRepository.deleteById(toDo.getId());
        toDoRepository.save(toDo);
        return "put";
    }
}
