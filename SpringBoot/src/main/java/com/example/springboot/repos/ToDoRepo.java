package com.example.springboot.repos;

import com.example.springboot.domain.ToDo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ToDoRepo extends CrudRepository<ToDo, Integer>{
    List<ToDo> findByPlace(String place);
}
