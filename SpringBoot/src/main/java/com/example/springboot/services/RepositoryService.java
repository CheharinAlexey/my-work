package com.example.springboot.services;


import com.example.springboot.domain.ToDo;
import com.example.springboot.repos.ToDoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;
@Service
public class RepositoryService {
    @Autowired
    private ToDoRepo toDoRepository;

    public ToDo save(ToDo toDo) {
        return toDoRepository.save(toDo);
    }

    public void saveAll(Iterable<ToDo> toDos) {
        toDoRepository.saveAll(toDos);
    }

    public Iterable<ToDo> findAll() {
        return toDoRepository.findAll();
    }

    public Iterable<ToDo> findAllById(Iterable<Integer> id) {
        return toDoRepository.findAllById(id);
    }

    public Optional<ToDo> findById(int id) {
        return toDoRepository.findById(id);
    }

    public long count() {
        return toDoRepository.count();
    }

    public Optional<ToDo> get(int id) {
        return toDoRepository.findById(id);
    }

    public void deleteAll() {
        toDoRepository.deleteAll();
    }

    public void deleteAll(Iterable<ToDo> toDos) {
        toDoRepository.deleteAll(toDos);
    }

    public void deleteById(int id) {
        toDoRepository.deleteById(id);
    }

    public void delete(ToDo toDo) {
        toDoRepository.delete(toDo);
    }

    public List<ToDo> findByPlace(String place) {
        return toDoRepository.findByPlace(place);
    }

}
